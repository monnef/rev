# rev
Simple version of `rev` command for Linux and Windows. Only supports standard input (STDIN).

This command reads lines and every line reverses.

## Example
```sh
$ echo -e "123\nahoj" | stack exec rev
321
joha
```

## Download
Navigate to the [Tags](https://gitlab.com/monnef/rev/tags) and then download a binary for your OS by clicking on a link with paperclip.

## Development
Requirements: [Stack](http://haskellstack.org)

### Build
```
stack build
```

### Testing
```
stack test
```

### Running
With [stack-run](https://hackage.haskell.org/package/stack-run):
```
stack run
```

Without:
```
stack exec rev
```

## License
MIT
