module Lib
    ( reverseLines
    ) where

import Data.Function ((&))
import Control.Lens.Operators ((<&>))

reverseLines :: String -> String
reverseLines xs = xs & lines <&> reverse & unlines
